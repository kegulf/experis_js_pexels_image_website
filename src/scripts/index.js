import "../styles/index.scss";

const apiKey = "563492ad6f91700001000001df972917553a4d6881190c9ea2cd744c";

// Lovely state is lovely
const state = {
	query: "kittens",
	perPage: "15",
	currentPage: "1",
	totalPages: "0"
};

$(document).ready(() => {
	$("#ten-back-link").click(jumpTenPagesBackwards);
	$("#prev-page-link").click(previousPage);
	$("#next-page-link").click(nextPage);
	$("#ten-forward-link").click(jumpTenPagesForward);

	$("#search-button").on("click", () => {
		state.query = $("#search-input").val();

		fetchPictures();
	});

	$("#photos-per-page-select").change(function() {
		state.perPage = $(this)
			.children("option:selected")
			.val();

		console.log(state);

		fetchPictures();
	});

	fetchPictures();
});

const fetchPictures = () => {
	$.ajax({
		url: getApiUrl(state),
		type: "GET",

		headers: { Authorization: `Bearer ${apiKey}` }
	}).done(data => {
		state.totalPages = Math.ceil(data.total_results / state.perPage);

		renderPhotos(data.photos);
		updatePaginationButtons();
	});
};

const updatePaginationButtons = () => {
	if (parseInt(state.currentPage) <= 10) {
		$("#ten-back-li").addClass("disabled");
	} else {
		$("#ten-back-li").removeClass("disabled");
	}

	if (state.currentPage === "1") {
		$("#prev-page-li").addClass("disabled");
	} else {
		$("#prev-page-li").removeClass("disabled");
	}

	if (state.currentPage === state.totalPages) {
		$("#next-page-li").addClass("disabled");
	} else {
		$("#next-page-li").removeClass("disabled");
	}

	if (state.currentPage >= state.totalPages - 10) {
		$("#ten-forward-li").addClass("disabled");
	} else {
		$("#ten-forward-li").removeClass("disabled");
	}

	$("#current-page-span").html(`${state.currentPage}/${state.totalPages}`);
};

const jumpTenPagesBackwards = () => {
	state.currentPage -= 10;
	fetchPictures();
};
const jumpTenPagesForward = () => {
	state.currentPage = parseInt(state.currentPage) + 10;
	fetchPictures();
};
const nextPage = () => {
	state.currentPage = parseInt(state.currentPage) + 1;
	fetchPictures();
};
const previousPage = () => {
	state.currentPage -= 1;
	fetchPictures();
};

const createSingePaginationButton = (description, displayText, disabled, id) => {
	return `<li class="page-item ${disabled ? "disabled" : ""}">
      <a class="page-link" href="#" aria-label="${description}" id="${id}">
        <span aria-hidden="true">${displayText}</span>
        <span class="sr-only">${description}</span>
      </a>
    </li>`;
};

const getApiUrl = state => {
	const { query, perPage, currentPage } = state;

	return `https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=${query}&per_page=${perPage}&page=${currentPage}`;
};

const renderPhotos = photos => {
	$(".result-container").empty();

	$.each(photos, (i, photo) => {
		$(".result-container").append(createCard(photo));
	});
};

const createCard = photo => {
	return `<div class="card m-2" style="width: 10rem;">
        <img src="${photo.src.portrait}" class="card-img-top" alt="A photo of ${state.query}">
        <div class="card-body">
            <p class="card-text">Photo by ${photo.photographer}</p>
        </div>
    </div>`;
};
